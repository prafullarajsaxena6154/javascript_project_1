
const AUDI_BMW = (car_array) =>
{
    if(Array.isArray(car_array))
    {
        if(car_array.length==0)
        {
            return [];
        }
    }
    else if(!Array.isArray(car_array))
    {
        return [];
    }
    let only_AUDI_BMW = new Array();

    for(let index = 0 ; index < car_array.length ; index++)
    {
        if(car_array[index].car_make === 'Audi' || car_array[index].car_make === 'BMW')
        {
            only_AUDI_BMW.push(car_array[index])
        }
    }
    return only_AUDI_BMW;
}

module.exports = AUDI_BMW;