
const car_data = (car_array, id) => {

    //console.log(car_array);
    //console.log(id);
    if(Array.isArray(car_array))
    {
        //console.log(1);   
        if(car_array.length===0)
        {
            //console.log(2);
            return [];
        }
    }
    if(id === null || id === undefined || !Number.isInteger(id))
    {
        //console.log(1);
        return [];
    }
    if(!Array.isArray(car_array))
    {
        //console.log(11);
        return [];
    }
        for(let index = 0; index<car_array.length; index++)
        {
            if(car_array[index].id === id)
            {
                //console.log(22);
                return(car_array[index]);
            }
        }
};

module.exports = car_data;