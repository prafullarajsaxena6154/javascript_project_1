
const sorted_data_car_model = (car_array) => {
    if(Array.isArray(car_array))
    {
        if(car_array.length==0)
        {
            return [];
        }
    }
    else if(!Array.isArray(car_array))
    {
        return [];
    }
        function compare( a, b ) {
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase())
        {
            return -1;
        }
        if(a.car_model.toLowerCase() > b.car_model.toLowerCase())
        {
            return 1;
        }
        return 0;
    }
    
    car_array.sort(compare);
    return(car_array);
}

module.exports = sorted_data_car_model;